<?php include_once ("../../lib/connection.php");?>
<?php include_once ("../../lib/setting.php");?>
<?php include_once ("../elements/header.php");?>
<?php include_once ("../elements/nav.php");?>
<?php include_once('../elements/aside.php'); ?>

<div id="page-wrapper">
    <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Student</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
    <div class="row">
        <div class=" col-md-offset-3 col-md-6">
            <form action="views/student/store.php" method="post">
                <div class="form-group">
                    <label for="first_name">First Name :</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter Your First Name" required="">
                </div>

                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Your Last Name" required="">
                </div>
                <div class="form-group">
                    <label for="seip">STUDENT ID</label>
                    <input type="text" class="form-control" id="seip" name="seip" placeholder="Enter Your SEIP ID"  required="">
                </div>
                <button onclick="myFunction()"type="submit" class="btn btn-info"  required="" >Submit</button>
                
            </form>
        </div>
    </div>
</div>
<script>
      function myFunction() {
              alert("Data has been saved sucessfully!");
          }
 </script>

<?php include_once('../elements/footer.php');?>