

<?php
include_once ("../../lib/connection.php");
include_once ("../../lib/setting.php");

//build query
//$query = "SELECT * FROM `students` ORDER BY id DESC LIMIT 0,5";
$query = "SELECT * FROM `students` ";
//execution
$stmt = $db->query($query);
$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<?php include_once ("../elements/header.php");?>
<?php include_once ("../elements/nav.php");?>
<?php include_once ("../elements/aside.php");?>
 <div id="page-wrapper">
    <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Students info</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
    
    <div class="row">
        <div class=" col-lg-12">
        
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>

                        <th>Sr. No. </th>
                        <!-- <th>ID</th> -->
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Student Id </th>
                        <th>Courses</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $counter = 1;
                    foreach($students as $student):




                        $query = "SELECT course_id FROM map_students_courses WHERE student_id=".$student['id'];
                        $stmt = $db->query($query);
                        $course_ids = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        /*
                         * TODO
                         * Join Query
                         * IN
                         */

                        $course_titles = [];
                        foreach($course_ids as $course_id){
                            $query = "SELECT title FROM courses WHERE id=".$course_id['course_id'];
                            $stmt = $db->query($query);
                            $course_titles[] = $stmt->fetch(PDO::FETCH_ASSOC)['title'];
                        }

                        $courses = implode(',',$course_titles);



                ?>
                    <tr>
                        <td><?php echo $counter++;?></td>
                        <!-- <td><?php echo $student['id']?></td> -->
                        <td><?php echo $student['first_name']?></td>
                        <td><?php echo $student['last_name']?></td>
                        <td><?php echo $student['seip']?></td>
                        <td><?php echo $courses;?></td>

                        <td>
                            <a href="views/student/show.php?id=<?=$student['id']?>">Show</a> |
                            <a href="views/student/edit.php?id=<?=$student['id']?>">Edit</a> |
                            <a href="views/student/delete.php?id=<?=$student['id']?>"onclick="myFunction()">Delete</a> |
                            <script>
                                  function myFunction() {
                                          alert("Data has been delete sucessfully!");
                                      }
                             </script>
                            <a href="views/student/assign_edit.php?id=<?=$student['id']?>">Edit Courses</a>
                        </td>
                    </tr>
                <?php
                    endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>

 </div>
        <!-- /#page-wrapper -->

<?php include_once("../elements/footer.php");?>


