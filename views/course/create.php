
<?php include_once('../../lib/setting.php');?>

<?php include_once('../elements/header.php');?>
<?php include_once('../elements/aside.php');?>
<div id="page-wrapper">
    <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New Course</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
    <div class="row">
       <div class=" col-md-offset-1 col-md-10">
            <form action="views/course/store.php" method="post">
                <div class="form-group">
                    <label for="title">Title :</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Course Title" required="">
                </div>
                <div class="form-group">
                    <label for="code">Code</label>
                    <input type="text" class="form-control" id="code" name="code" placeholder="Enter Course Code" required="">
                </div>

                <button type="submit" class="btn btn-info">Submit</button>
            </form>
        </div>
    </div>
</div>



<?php include_once('../elements/footer.php');?>

