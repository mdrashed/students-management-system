<?php include_once('../../lib/connection.php'); ?>
<?php include_once('../../lib/setting.php');?>


<?php include_once("../elements/header.php");?>

    <div id="frm">
    	<form action="views/login/process.php" method="post">
    		<p>
    			<label>Username</label>
    			<input type="text" id="username" name="username" placeholder="username">
    		</p>
    		<p>
    			<label>Password</label>
    			<input type="Password" id="password" name="password" placeholder="password">
    		</p>
    		<p>
    			
    			<input type="submit" id="btn" value="login">
    		</p>
    	</form>
    </div>

  </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

</body>

</html>
